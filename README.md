# French Fracas Test Suite

This repository contains the French version of the FraCas test suite.

It is based on the [MultiFraCas](https://github.com/GU-CLASP/multifracas) test suite which provides FraCas translation for several languages.

It is licensed under the Creative Commons BY-NC-SA [<img alt="CC BY-NC-SA" title="CC BY-NC-SA" src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc-sa.png" height="20">](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

## French translation

The French version is available in the `files` folder and is described in:

Maxime Amblard, Clément Beysson, Philippe de Groote, Bruno Guillaume, Sylvain Pogodalla.
[A French Version of the FraCaS Test Suite](https://aclanthology.org/2020.lrec-1.721). LREC 2020

## Semantic annotation

A subpart of the French FraCaS problems are semantically annotated.
The files are available in the `semantics` folder.